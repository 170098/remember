using Foundation;
using System;
using UIKit;

namespace remember
{
    public partial class CreateProfileViewController : UIViewController
    {
        public CreateProfileViewController (IntPtr handle) : base (handle)
        {
        }

        partial void NextButton_TouchUpInside(CustomButton sender)
        {
            PerformSegue("toEmergencyContacts", null);
        }
    }
}