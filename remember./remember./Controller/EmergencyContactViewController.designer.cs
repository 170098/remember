// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace remember
{
    [Register ("EmergencyContactViewController")]
    partial class EmergencyContactViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton BackButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField EmergencyCellphoneNumberTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField EmergencyFullNameTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField EmergencyRelationshipTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        remember.CustomButton NextButton { get; set; }

        [Action ("BackButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void BackButton_TouchUpInside (UIKit.UIButton sender);

        [Action ("NextButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void NextButton_TouchUpInside (remember.CustomButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (BackButton != null) {
                BackButton.Dispose ();
                BackButton = null;
            }

            if (EmergencyCellphoneNumberTextField != null) {
                EmergencyCellphoneNumberTextField.Dispose ();
                EmergencyCellphoneNumberTextField = null;
            }

            if (EmergencyFullNameTextField != null) {
                EmergencyFullNameTextField.Dispose ();
                EmergencyFullNameTextField = null;
            }

            if (EmergencyRelationshipTextField != null) {
                EmergencyRelationshipTextField.Dispose ();
                EmergencyRelationshipTextField = null;
            }

            if (NextButton != null) {
                NextButton.Dispose ();
                NextButton = null;
            }
        }
    }
}