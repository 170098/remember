// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace remember
{
    [Register ("CreateProfileViewController")]
    partial class CreateProfileViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField CellphoneNumberTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIDatePicker DateOfBirthPicker { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField DateOfBirthTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField FullNameTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        remember.CustomButton NextButton { get; set; }

        [Action ("NextButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void NextButton_TouchUpInside (remember.CustomButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (CellphoneNumberTextField != null) {
                CellphoneNumberTextField.Dispose ();
                CellphoneNumberTextField = null;
            }

            if (DateOfBirthPicker != null) {
                DateOfBirthPicker.Dispose ();
                DateOfBirthPicker = null;
            }

            if (DateOfBirthTextField != null) {
                DateOfBirthTextField.Dispose ();
                DateOfBirthTextField = null;
            }

            if (FullNameTextField != null) {
                FullNameTextField.Dispose ();
                FullNameTextField = null;
            }

            if (NextButton != null) {
                NextButton.Dispose ();
                NextButton = null;
            }
        }
    }
}