using Foundation;
using System;
using UIKit;

namespace remember
{
    public partial class EmergencyContactViewController : UIViewController
    {
        public EmergencyContactViewController (IntPtr handle) : base (handle)
        {
        }

        partial void NextButton_TouchUpInside(CustomButton sender)
        {
            PerformSegue("toMedicalProfile", null);
        }

        partial void BackButton_TouchUpInside(UIButton sender)
        {
            NavigationController.PopViewController(true);
        }
    }
}