using Foundation;
using System;
using UIKit;

namespace remember
{
    public partial class MotivationalNotificationsViewController : UIViewController
    {
        public MotivationalNotificationsViewController (IntPtr handle) : base (handle)
        {
        }

        partial void SkipButton_TouchUpInside(UIButton sender)
        {
            
        }

        partial void NextButton_TouchUpInside(CustomButton sender)
        {
            
        }

        partial void BackButton_TouchUpInside(UIButton sender)
        {
            NavigationController.PopViewController(true);
        }
    }
}