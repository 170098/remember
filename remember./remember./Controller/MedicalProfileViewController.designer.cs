// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace remember
{
    [Register ("MedicalProfileViewController")]
    partial class MedicalProfileViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField AllergiesTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton BackButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField BloodTypeTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIStackView CustomSegmentStackView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField HeightTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel MedicalAidLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITapGestureRecognizer MedicalAidTapGesture { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField MembershipNumberTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        remember.CustomTextView MembershipNumberView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        remember.CustomButton NextButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel NoMedicalAidLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITapGestureRecognizer NoMedicalAidTapGesture { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField PlanTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        remember.CustomTextView PlanView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField ProviderTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        remember.CustomTextView ProviderView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint SelectedSegmentLeftConstraint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint SelectedSegmentRightConstraint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        remember.CustomTextView SelectedSegmentView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView SpacerView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField WeightTextField { get; set; }

        [Action ("BackButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void BackButton_TouchUpInside (UIKit.UIButton sender);

        [Action ("NextButton_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void NextButton_TouchUpInside (remember.CustomButton sender);

        [Action ("OnMedicalAidTap:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void OnMedicalAidTap (UIKit.UITapGestureRecognizer sender);

        [Action ("OnNoMedicalAidTap:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void OnNoMedicalAidTap (UIKit.UITapGestureRecognizer sender);

        void ReleaseDesignerOutlets ()
        {
            if (AllergiesTextField != null) {
                AllergiesTextField.Dispose ();
                AllergiesTextField = null;
            }

            if (BackButton != null) {
                BackButton.Dispose ();
                BackButton = null;
            }

            if (BloodTypeTextField != null) {
                BloodTypeTextField.Dispose ();
                BloodTypeTextField = null;
            }

            if (CustomSegmentStackView != null) {
                CustomSegmentStackView.Dispose ();
                CustomSegmentStackView = null;
            }

            if (HeightTextField != null) {
                HeightTextField.Dispose ();
                HeightTextField = null;
            }

            if (MedicalAidLabel != null) {
                MedicalAidLabel.Dispose ();
                MedicalAidLabel = null;
            }

            if (MedicalAidTapGesture != null) {
                MedicalAidTapGesture.Dispose ();
                MedicalAidTapGesture = null;
            }

            if (MembershipNumberTextField != null) {
                MembershipNumberTextField.Dispose ();
                MembershipNumberTextField = null;
            }

            if (MembershipNumberView != null) {
                MembershipNumberView.Dispose ();
                MembershipNumberView = null;
            }

            if (NextButton != null) {
                NextButton.Dispose ();
                NextButton = null;
            }

            if (NoMedicalAidLabel != null) {
                NoMedicalAidLabel.Dispose ();
                NoMedicalAidLabel = null;
            }

            if (NoMedicalAidTapGesture != null) {
                NoMedicalAidTapGesture.Dispose ();
                NoMedicalAidTapGesture = null;
            }

            if (PlanTextField != null) {
                PlanTextField.Dispose ();
                PlanTextField = null;
            }

            if (PlanView != null) {
                PlanView.Dispose ();
                PlanView = null;
            }

            if (ProviderTextField != null) {
                ProviderTextField.Dispose ();
                ProviderTextField = null;
            }

            if (ProviderView != null) {
                ProviderView.Dispose ();
                ProviderView = null;
            }

            if (SelectedSegmentLeftConstraint != null) {
                SelectedSegmentLeftConstraint.Dispose ();
                SelectedSegmentLeftConstraint = null;
            }

            if (SelectedSegmentRightConstraint != null) {
                SelectedSegmentRightConstraint.Dispose ();
                SelectedSegmentRightConstraint = null;
            }

            if (SelectedSegmentView != null) {
                SelectedSegmentView.Dispose ();
                SelectedSegmentView = null;
            }

            if (SpacerView != null) {
                SpacerView.Dispose ();
                SpacerView = null;
            }

            if (WeightTextField != null) {
                WeightTextField.Dispose ();
                WeightTextField = null;
            }
        }
    }
}