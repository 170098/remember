﻿using System;

using UIKit;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using ImageIO;
using System.Linq;
using VideoToolbox;
using CoreGraphics;

namespace remember
{
    public partial class ViewController : UIViewController
    {

        private UIPageViewController pageViewController;
        private List<string> _pageTitles;
        private List<string> _pageDescriptions;
        private List<string> _images;

        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            _pageTitles = new List<string>
            {
                "always remember to take your meds.",
                "create a medical profile.",
                "always stay motivated.",
                "everything is saved on device."
            };

            _pageDescriptions = new List<string>
            {
                "remember is a tool that reminds and motivates you.",
                "remember can also be used to assist first responders to access your medical information.",
                "remember will send you a daily motivational notification",
                "remember does not store any of your information on the cloud."
            };

            _images = new List<string>
            {
                "remember",
                "profile",
                "fitness2",
                "on-device"
            };

            pageViewController = this.Storyboard.InstantiateViewController("OnboardingPageViewController") as UIPageViewController;
            pageViewController.DataSource = new PageViewControllerDataSource(this, _pageTitles);

            var startVC = this.ViewControllerAtIndex(0) as ContentViewController;
            var viewControllers = new UIViewController[] { startVC };

            pageViewController.SetViewControllers(viewControllers, UIPageViewControllerNavigationDirection.Forward, false, null);
            pageViewController.View.Frame = new CGRect(0, 0, this.View.Frame.Width, this.View.Frame.Size.Height - 50);
            AddChildViewController(this.pageViewController);
            View.AddSubview(this.pageViewController.View);
            pageViewController.DidMoveToParentViewController(this);
            
        }

        public UIViewController ViewControllerAtIndex(int index)
        {
            var vc = this.Storyboard.InstantiateViewController("ContentViewController") as ContentViewController;
            vc.titleText = _pageTitles.ElementAt(index);
            vc.descriptionText = _pageDescriptions.ElementAt(index);
            vc.imageFile = _images.ElementAt(index);
            vc.pageIndex = index;
            return vc;
        }

        private class PageViewControllerDataSource : UIPageViewControllerDataSource
        {
            private ViewController _parentViewController;
            private List<string> _pageTitles;

            public PageViewControllerDataSource(UIViewController parentViewController, List<string> pageTitles)
            {
                _parentViewController = parentViewController as ViewController;
                _pageTitles = pageTitles;
            }

            public override UIViewController GetPreviousViewController(UIPageViewController pageViewController, UIViewController referenceViewController)
            {
                var vc = referenceViewController as ContentViewController;
                var index = vc.pageIndex;

                if (index == 0)
                {
                    return null;
                }
                else
                {
                    index--;
                    return _parentViewController.ViewControllerAtIndex(index);
                }
            }

            public override UIViewController GetNextViewController(UIPageViewController pageViewController, UIViewController referenceViewController)
            {
                var vc = referenceViewController as ContentViewController;
                var index = vc.pageIndex;

                index++;
                if (index == _pageTitles.Count)
                {
                    return null;
                }
                else
                {
                    return _parentViewController.ViewControllerAtIndex(index);
                }
            }

            public override nint GetPresentationCount(UIPageViewController pageViewController)
            {
                return _pageTitles.Count;
            }

            public override nint GetPresentationIndex(UIPageViewController pageViewController)
            {
                return 0;
            }
        }
    }
}