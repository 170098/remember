using Foundation;
using System;
using UIKit;

namespace remember
{
    public partial class OnboardingPageViewController : UIPageViewController, IUIPageViewControllerDataSource, IUIPageViewControllerDelegate
    {
     
        public OnboardingPageViewController (IntPtr handle) : base (handle)
        {
        }

        private UIViewController NewVC(string ViewController)
        {
            return UIStoryboard.FromName("Main", null).InstantiateViewController(ViewController);
        }

        UIViewController IUIPageViewControllerDataSource.GetNextViewController(UIPageViewController pageViewController, UIViewController referenceViewController)
        {
            throw new NotImplementedException();
        }

        UIViewController IUIPageViewControllerDataSource.GetPreviousViewController(UIPageViewController pageViewController, UIViewController referenceViewController)
        {
            throw new NotImplementedException();
        }
    }
}