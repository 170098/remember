using Foundation;
using System;
using UIKit;

namespace remember
{
    public partial class ContentViewController : UIViewController
    {
        public int pageIndex = 0;
        public string titleText;
        public string descriptionText;
        public string imageFile;

        public ContentViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            imageView.Image = UIImage.FromBundle(imageFile);
            titleLabel.Text = titleText;
            descriptionLabel.Text = descriptionText;

            if (pageIndex != 3)
            {
                nextButton.Hidden = true;
            } else
            {
                nextButton.Hidden = false;
            }
        }
    }
}