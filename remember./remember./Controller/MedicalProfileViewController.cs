using Foundation;
using System;
using UIKit;

namespace remember
{
    public partial class MedicalProfileViewController : UIViewController
    {
        public MedicalProfileViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            SelectedSegmentRightConstraint.Constant = CustomSegmentStackView.Frame.Width / 2f;
        }

        partial void NextButton_TouchUpInside(CustomButton sender)
        {
            PerformSegue("toMotivationalNotifications", null);
        }

        partial void BackButton_TouchUpInside(UIButton sender)
        {
            NavigationController.PopViewController(true);
        }

        partial void OnNoMedicalAidTap(UITapGestureRecognizer sender)
        {
            ShowOrHideMedicalAidFields(false);
        }

        partial void OnMedicalAidTap(UITapGestureRecognizer sender)
        {
            ShowOrHideMedicalAidFields(true);
        }

        void ShowOrHideMedicalAidFields(bool show)
        {
            if (show)
            {
                ProviderView.Hidden = false;
                MembershipNumberView.Hidden = false;
                PlanView.Hidden = false;
                SpacerView.Hidden = false;
                MedicalAidLabel.TextColor = UIColor.White;
                NoMedicalAidLabel.TextColor = UIColor.FromName("healthyOrange");

                UIView.Animate(5f, 0, UIViewAnimationOptions.CurveEaseInOut, () =>
                {
                    SelectedSegmentRightConstraint.Constant = CustomSegmentStackView.Frame.Width / 2f;
                    SelectedSegmentLeftConstraint.Constant = 0;
                }, null);

            } else
            {
                ProviderView.Hidden = true;
                MembershipNumberView.Hidden = true;
                PlanView.Hidden = true;
                SpacerView.Hidden = true;
                MedicalAidLabel.TextColor = UIColor.FromName("healthyOrange");
                NoMedicalAidLabel.TextColor = UIColor.White;

                UIView.Animate(5.0, 5.0, UIViewAnimationOptions.BeginFromCurrentState, () =>
                {
                    SelectedSegmentRightConstraint.Constant = 0;
                    SelectedSegmentLeftConstraint.Constant = CustomSegmentStackView.Frame.Width / 2f;
                }, null);
            }
        }
    }
}