using Foundation;
using System;
using UIKit;

namespace remember
{
    public partial class CustomButton : UIButton
    {
        public CustomButton (IntPtr handle) : base (handle)
        {
        }

        [Export("awakeFromNib")]
        public override void AwakeFromNib()
        {
            Layer.CornerRadius = 10f;
        }
    }
}