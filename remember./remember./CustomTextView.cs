using Foundation;
using System;
using UIKit;

namespace remember
{
    public partial class CustomTextView : UIView
    {
        public CustomTextView (IntPtr handle) : base (handle)
        {
        }

        [Export("awakeFromNib")]
        public override void AwakeFromNib()
        {
            Layer.CornerRadius = 10f;
        }
    }
}